(function (KoreSDK) {

    var KoreSDK = KoreSDK || {};

    var url = window.location.href;

    var botOptions = {};
    botOptions.logLevel = 'debug';
    botOptions.koreAPIUrl = "https://bots.kore.ai/api/";
    botOptions.koreSpeechAPIUrl = "";//deprecated
    //botOptions.bearer = "bearer xyz-------------------";
    //botOptions.ttsSocketUrl = '';//deprecated
    botOptions.koreAnonymousFn = koreAnonymousFn;
    botOptions.recorderWorkerPath = '../libs/recorderWorker.js';

    botOptions.JWTUrl = "https://qa-kml.carestreamdental.com/api/JWT/Token";
    botOptions.userIdentity = 'anthony.demeusy@csdental.com';// Provide users email id here
    botOptions.botInfo = { name: "Cassidy Dev VA", "_id": "st-69d076a1-a572-5523-93c4-282ae5208baf" }; // bot name is case sensitive
    botOptions.clientId = "cs-70d03d26-cd89-5029-b578-b98a0d353221";
    botOptions.clientSecret = "OJdXCXxa4q9aecAc8RbPqdxZNnKKHnH5UmuYSBHqftc=";
    botOptions.userLocale = url;
    // To modify the web socket url use the following option
    // botOptions.reWriteSocketURL = {
    //     protocol: 'PROTOCOL_TO_BE_REWRITTEN',
    //     hostname: 'HOSTNAME_TO_BE_REWRITTEN',
    //     port: 'PORT_TO_BE_REWRITTEN'
    // };

    var chatConfig = {
        botOptions: botOptions,
        allowIframe: false, 			// set true, opens authentication links in popup window, default value is "false"
        isSendButton: false, 			// set true, to show send button below the compose bar
        isTTSEnabled: false,			// set true, to hide speaker icon
        ttsInterface: 'webapi',        // webapi or awspolly , where default is webapi
        isSpeechEnabled: false,			// set true, to hide mic icon
        allowGoogleSpeech: true,		// set true, to use Google speech engine instead KORE.AI engine.This feature requires valid Google speech API key. (Place it in 'web-kore-sdk/libs/speech/key.js')
        allowLocation: true,			// set false, to deny sending location to server
        loadHistory: true,				// set true to load recent chat history
        messageHistoryLimit: 10,		// set limit to load recent chat history
        autoEnableSpeechAndTTS: false, 	// set true, to use talkType voice keyboard.
        graphLib: "d3",				// set google, to render google charts.This feature requires loader.js file which is available in google charts documentation.
        googleMapsAPIKey: "",
        minimizeMode: true,             // set true, to show chatwindow in minimized mode, If false is set remove #chatContainer style in chatwindow.css  
        multiPageApp: {
            enable: false,              //set true for non SPA(Single page applications)
            userIdentityStore: 'localStorage',//'localStorage || sessionStorage'
            chatWindowStateStore: 'localStorage'//'localStorage || sessionStorage'
        },
        supportDelayedMessages: true,    // enable to add support for renderDelay in message nodes which will help to render messages with delay from UI       
        pickersConfig: {
            showDatePickerIcon: false,           //set true to show datePicker icon
            showDateRangePickerIcon: false,      //set true to show dateRangePicker icon
            showClockPickerIcon: false,          //set true to show clockPicker icon
            showTaskMenuPickerIcon: false,       //set true to show TaskMenu Template icon
            showradioOptionMenuPickerIcon: false //set true to show Radio Option Template icon
        }
    };
    /* 
       allowGoogleSpeech will use Google cloud service api.
       Google speech key is required for all browsers except chrome.
       On Windows 10, Microsoft Edge will support speech recognization.
    */

    KoreSDK.chatConfig = chatConfig;
})(window.KoreSDK);
