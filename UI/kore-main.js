(function ($) {
    var koremain = document.getElementById("koremain");
    var productCode = koremain.getAttribute("data-product");
    var isAuthenticated = koremain.getAttribute("data-auth");
    var newlyAuthenticated = false;
    var currentUrl = window.location.href;
    var encodedCurrentUrl = encodeURIComponent(currentUrl);
    var openChat = window.location.href.indexOf('openChat=1');
    if (openChat > -1) {
        newlyAuthenticated = true;
    } else {
        currentUrl = UpdateQueryString("openChat", "1", currentUrl);
        encodedCurrentUrl = encodeURIComponent(currentUrl);
    }

    var paths = window.location.pathname.split('/');
    var userLocale = paths[1];

    var ssoUri = "https://" + window.location.host;
    ssoUri += "/" + userLocale + "/AzureAccount/SignUpSignIn?redirectUrl=" + encodedCurrentUrl;

    var customData = {
        "product": productCode,
        "userlocale": userLocale,
        "ssoUri": ssoUri,
        "newAuth": newlyAuthenticated
    };

    if (isAuthenticated === "True") {
        var objectId = koremain.getAttribute("data-objectid");
        var firstName = koremain.getAttribute("data-first");
        var lastName = koremain.getAttribute("data-last");
        var email = koremain.getAttribute("data-email");
        var sap = koremain.getAttribute("data-sap");

        customData.objectId = objectId;
        customData.firstName = firstName;
        customData.lastName = lastName;
        customData.email = email;
        customData.sapNumber = sap;
    } else {
        customData.objectId = "";
        customData.firstName = "";
        customData.lastName = "";
        customData.email = "";
        customData.sapNumber = "";
    }

    $(document).ready(function () {
        function assertion(options, callback) {
            var jsonData = {
                "clientId": options.clientId,
                "clientSecret": options.clientSecret,
                "identity": options.userIdentity,
                "aud": "",
                "isAnonymous": !isAuthenticated
            };
            $.ajax({
                url: options.JWTUrl,
                type: 'post',
                data: jsonData,
                dataType: 'json',
                success: function (data) {
                    options.assertion = data.jwt;
                    options.botInfo.customData = customData;
                    options.handleError = koreBot.showError;
                    options.chatHistory = koreBot.chatHistory;
                    options.botDetails = koreBot.botDetails;
                    callback(null, options);
                    setTimeout(function () {
                        if (koreBot && koreBot.initToken) {
                            koreBot.initToken(options);
                        }
                    }, 2000);
                },
                error: function (err) {
                    koreBot.showError(err.responseText);
                }
            });
        }

        var chatConfig = window.KoreSDK.chatConfig;
        chatConfig.botOptions.assertionFn = assertion;

        var koreBot = koreBotChat();
        koreBot.show(chatConfig);
        $('.openChatWindow').click(function () {
            koreBot.show(chatConfig);
        });
        // CareStream automatically bot opens when looggedin user comes again 
        var openChat = window.location.href.indexOf('openChat');
        if (openChat > -1) {
            $('.minimized').trigger("click");
        }

        //if (localStorage.getItem("isLoggedinUser") == "true") {
        //    $('.minimized').trigger("click");
        //}
    });

})(jQuery || (window.KoreSDK && window.KoreSDK.dependencies && window.KoreSDK.dependencies.jQuery));

function UpdateQueryString(key, value, url) {
    if (!url) url = window.location.href;
    var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi"),
        hash;

    if (re.test(url)) {
        if (typeof value !== 'undefined' && value !== null) {
            return url.replace(re, '$1' + key + "=" + value + '$2$3');
        }
        else {
            hash = url.split('#');
            url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
            if (typeof hash[1] !== 'undefined' && hash[1] !== null) {
                url += '#' + hash[1];
            }
            return url;
        }
    }
    else {
        if (typeof value !== 'undefined' && value !== null) {
            var separator = url.indexOf('?') !== -1 ? '&' : '?';
            hash = url.split('#');
            url = hash[0] + separator + key + '=' + value;
            if (typeof hash[1] !== 'undefined' && hash[1] !== null) {
                url += '#' + hash[1];
            }
            return url;
        }
        else {
            return url;
        }
    }
}